Snoobi
------

What is Snoobi
Snoobi is a visitor tracking tool as well as a web analytics service. The visitor tracking collects information about your website, providing the insight necessary to ensure that your website supports your business objectives and helps in achieving them. With the guidance of Snoobi's professionals, this knowledge can be turned to action and action into results. (Source: www.snoobi.com)

This module offers a simple light weight integration to Snoobi making it easier to add Snoobi tracking instead of supporting it directly with custom module or in your theme.

Installation
1. Place snoobi folder in your sites modules directory
2. Activate Snoobi module from Administer -> Site building -> Modules
3. Set your Snoobi user account name from Administer -> Site configuration -> Snoobi
4. Grant 'view snoobi' permission to all roles that should be tracked at Administer -> User management -> Permissions
5. OPTIONAL: You may want to grant additional 'administer snoobi' permissions to some roles

That's it.

Maintainers
-----------
Vesa Palmu
Co-maintainers wanted!